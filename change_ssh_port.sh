#!/bin/bash

if [[ $# -ne 1 ]]
then
     echo "First parameter is the new SSHD port."
     exit 1
fi

echo "Change SSHD port to $1..."
sudo -- bash -c "echo 'Port $1' >> /etc/ssh/sshd_config"
sudo systemctl restart sshd
